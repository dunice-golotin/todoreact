import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
//import App from './containers/App'
import configureStore from './store/configureStore';
import { Router, browserHistory } from 'react-router'
import { routes } from './routes'
import './style.css'
const store = configureStore()

render(
  <Provider store={store}>
    <Router history={browserHistory} routes={routes} />
  </Provider>,
  document.getElementById('root')
)
