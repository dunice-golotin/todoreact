export function addNewString (lists) {
    return {
        type: 'NEW_STRING',
        payload: lists
    }
}