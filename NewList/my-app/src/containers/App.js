import React, { Component } from 'react'
import { connect } from 'react-redux' 
import { bindActionCreators } from 'redux'
import * as pageActions from '../actions/PageActions'
import './App.css'
class App extends Component {
  render() {
    return <div className='project'>
      {this.props.children}
    </div>
  }
}
function mapStateToProps(state) {
  return {
    user: state.user,
    page: state.page
  }
}
function mapDispatchToProps(dispatch) {
  return {
    pageActions: bindActionCreators(pageActions, dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(App)