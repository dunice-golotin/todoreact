import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as pageActions from '../../actions/AddNewString';
import './style.css';
import Contact from '../Contact';
import PropTypes from 'prop-types'; 
class ContactList extends Component {
    constructor(props) {
        super(props)
        this.data = this.props.list.message;
        this.setString = this.props.pageActions.addNewString;
        this.addNewContact = this.addNewContact.bind(this);
        
        this.state ={
            hide : false,
        }
    }
    deleteItem(index) {
        this.data.splice(index, 1);
        localStorage.setItem('lists', JSON.stringify(this.data));
        this.setString(this.data);
    }
    addNewContact(){
        this.context.router.push(`/additem`)
    }
    hideAll(){
        if(this.state.hide){
            this.setState({hide:false})
        }
        else{
            this.setState({hide:true})
        }
    }
    render() {
        let context = this;
        var template;
        
        if (this.data) {
            template = this.data.map(function (item, index) {
                let data = {index:index, hide: context.state.hide}
                return <div  key={index}>
                    <Contact data={data}  /> 
                </div>
            })
        }

        return <div>
        <button className="control-button" onClick={this.addNewContact}>Add new contact</button>
        <button className="control-button" onClick={this.hideAll.bind(this)}>Display All/favorite</button>
        <div className={'news'}>{template}</div>
        </div>
    }
}
function mapStateToProps(state) {
    return {
        list: state.list
    }
}
function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators(pageActions, dispatch)
    }
}
ContactList.contextTypes = {
    router: PropTypes.object.isRequired
}
export default connect(mapStateToProps, mapDispatchToProps)(ContactList)
