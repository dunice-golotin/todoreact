import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as pageActions from '../../actions/AddNewString';
import PropTypes from 'prop-types'; 
import {Link} from 'react-router'
class Contact extends Component {
    constructor(props) {
        super(props)

        this.data = this.props.list.message
        this.item = this.data[this.index];
        this.setString = this.props.pageActions.addNewString;
        this.addNewContact = this.addNewContact.bind(this);
        
        this.state ={
            favorite : false,
            hide: false
        }
    }
    deleteItem(index) {
        this.data.splice(index, 1);
        localStorage.setItem('lists', JSON.stringify(this.data));
        this.setString(this.data);
    }
    addNewContact(){
        this.this.router.push(`/additem`)
    }
    setFavoriteClass(){
        if(this.state.favorite){
            this.setState({favorite:false})
        }
        else{
            this.setState({favorite:true})
        }
    }
    render() {
        this.index = this.props.data.index;
        this.data = this.props.list.message
        this.item = this.data[this.index];
        let author=this.item.author,
            usermail=this.item.usermail,
            number=this.item.number;
        return <div className={(this.state.favorite ? 'favorite': '')+' string '+(this.props.data.hide ? 'hide' : '')}>
            <div className="field author" >{author}</div>
            <div className="field usermail " >{usermail}</div>
            <div className="field number" >{number}</div>
            <button className="button-list" onClick={this.deleteItem.bind(this, this.index)}>del</button>
            <button className="button-list"><Link className="link" to={'/additem/'+this.index} name={this.data}>Update</Link></button>
            <button className="button-list" onClick={this.setFavoriteClass.bind(this)}>favorite</button>
        </div>
    }
}
function mapStateToProps(state) {
    return {
        list: state.list
    }
}
function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators(pageActions, dispatch)
    }
}
Contact.contextTypes = {
    router: PropTypes.object.isRequired
}
export default connect(mapStateToProps, mapDispatchToProps)(Contact)