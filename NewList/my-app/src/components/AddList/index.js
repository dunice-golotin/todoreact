import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as pageActions from '../../actions/AddNewString';
import PropTypes from 'prop-types';
class AddList extends Component {
    constructor(props) {
        super(props);
        this.lists = this.props.list.message;
        if (!this.props.params.param) {
            this.state = {
                authorIsEmpty: true,
                mailIsEmpty: true,
                numberIsEmpty: true,
            }

        } else {
            this.state = {
                authorIsEmpty: false,
                mailIsEmpty: false,
                numberIsEmpty: false
            }

        }
    }
    onInit() {
        if (this.props.params.param) {
            this.index = this.props.params.param;
            this.username = this.lists[this.index].author;
            this.usermail = this.lists[this.index].usermail;
            this.usernumber = this.lists[this.index].number;
        }
        else {
            this.username = '';
            this.usermail = '';
            this.usernumber = '';
        }
    }
    onFieldChange(fieldName, e) {
        if (e.target.value.trim().length > 0) {
            this.setState({ ['' + fieldName]: false })
        } else {
            this.setState({ ['' + fieldName]: true })
        }
    }
    onYearBtnClick() {
        let newString = {
            author: ReactDOM.findDOMNode(this.refs.username).value,
            usermail: ReactDOM.findDOMNode(this.refs.usermail).value,
            number: ReactDOM.findDOMNode(this.refs.usernumber).value
        }

        if (this.props.params.param) {
            [].splice()
            this.lists.splice(this.index, 1, newString)
        }
        else {
            this.lists.push(newString);
        }
        localStorage.setItem('lists', JSON.stringify(this.lists));
        this.context.router.push(`/`)
    }
    render() {
        this.onInit()
        return <div>
                <div><h1>Redacting lists of contact</h1></div>
                <div className='inputfild'><lable className='label-addlist'>Username</lable><input defaultValue={this.username} ref="username" onChange={this.onFieldChange.bind(this, 'authorIsEmpty')} /></div>
                <div className='inputfild'><lable className='label-addlist'>Usermail</lable><input defaultValue={this.usermail} ref="usermail" onChange={this.onFieldChange.bind(this, 'mailIsEmpty')} /></div>
                <div className='inputfild'><lable className='label-addlist'>Usernumber</lable><input defaultValue={this.usernumber} ref="usernumber" onChange={this.onFieldChange.bind(this, 'numberIsEmpty')} /></div>
                <button className="addbutton" type='submit' onClick={this.onYearBtnClick.bind(this)} disabled={
                    this.state.authorIsEmpty || this.state.mailIsEmpty || this.state.numberIsEmpty}>OK</button>
        </div>
    }
}
function mapStateToProps(state) {
    return {
        list: state.list
    }
}
function mapDispatchToProps(dispatch) {
    return {
        pageActions: bindActionCreators(pageActions, dispatch)
    }
}
AddList.contextTypes = {
    router: PropTypes.object.isRequired
}
export default connect(mapStateToProps, mapDispatchToProps)(AddList)