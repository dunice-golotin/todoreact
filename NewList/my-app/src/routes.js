import React from 'react'
import { Route, IndexRoute } from 'react-router'
import App from './containers/App';
import AddList from './components/AddList'
import ContactList from './components/ContactList';

//import UpgradeItem from './components/UpgradeItem'
export const routes = (
  <div>
    <Route path='/' component={App}>
      <IndexRoute component={ContactList} />
      <Route path='/additem' component={AddList}/>
      <Route path='/additem/:param' component={AddList}/>
    </Route>
    <Route path='*' component={App} />
  </div>
)
