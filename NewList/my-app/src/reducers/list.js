const initialState = {
    message: getMessage()
}
function getMessage(){
    if(localStorage.getItem('lists')){
        return JSON.parse(localStorage.getItem('lists'))
    }
    else {
        return [];
    }
}
export default function user(state = initialState,action) {
    switch (action.type) {
        case 'NEW_STRING':
            return { ...state, payload: action.payload }
        default:
            return state;
    }
}